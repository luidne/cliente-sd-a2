package cliente.restful.modelo;

import cliente.restful.conexao.RestClient;
import java.io.File;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class RotaDAORest {
	
	private RestClient conexao;
	private ObjectMapper mapper;
	
	public RotaDAORest(){
		this.conexao = new RestClient("musica/");
		this.mapper = new ObjectMapper();
	}
	
	public File buscarPorLinha(int idArquivo){				
		return this.conexao.getObject("id/"+idArquivo);
	}
	
	public List<File> buscarTudo(){
		
//		return this.conexao.getList("cruzam/"+jsonPonto1+"/"+jsonPonto2, Rota.class);
		return this.conexao.getList("tudo");
	}

}
