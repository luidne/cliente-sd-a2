package cliente.restful.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luídne Mota
 */
public class ControlaArquivos {

    /**
     * Grava arquivo. Sobrescreve arquivo se já existe.
     *
     * @param caminho o caminho completo do arquivo.
     * @param arquivo o arquivo enviado pelo PrimeFaces.
     * @return <strong>true</strong> se gravou.<br/>
     * @return <strong>false</strong> se não.
     */
    public static boolean gravarArquivo(String caminho) {
        File file = new File("f:/cache/" + caminho);
        boolean res = false;
        try {
            FileOutputStream fo = new FileOutputStream(file);
//            fo.write(arquivo.getContents());
            fo.close();

            res = true;
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            return res;
        }
    }

    public static void gravarArquivo(String caminho, InputStream in) {
        FileOutputStream out;
        try {
            out = new FileOutputStream(Constantes.caminhoContexto+"/"+caminho);

            int b;
            while ((b = in.read()) > -1) {
                out.write(b);
            }
            in.close();
            out.close();
        } catch (Exception ex) {
            Logger.getLogger(ControlaArquivos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
