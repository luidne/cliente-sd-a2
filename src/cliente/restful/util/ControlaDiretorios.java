package cliente.restful.util;

import java.io.File;

/**
 *
 * @author Luídne Mota
 */
public class ControlaDiretorios
{

    static File dir;

    public ControlaDiretorios(){}

    public ControlaDiretorios(String caminho)
    {
        criaDiretorio(caminho);
    }

    /**
     * *
     *
     * @param caminho
     * @return
     */
    public static boolean criaDiretorio(String caminho)
    {
        dir = new File(Constantes.caminhoContexto+caminho);

        return dir.mkdirs();
    }

    /**
     * Cria diretório a partir do contexto da aplicação.
     *
     * @param caminho Caminho da pasta a ser criada.
     * @return <strong>1</strong> se criou o diretório.<br/>
     * @return <strong>2</strong> se o diretório já existe.<br/>
     * @return <strong>0</strong> se não criou o diretório.<br/>
     * @return <strong>-1</strong> se nenhuma das opções anteriores.<br/>
     */
    public static int criaDiretorio2(String caminho)
    {
        dir = new File(Constantes.caminhoContexto+caminho);

        if (dir.mkdirs())
        {
            return 1;
        } else if (dir.exists())
        {
            return 2;
        } else if (!dir.mkdirs())
        {
            return 0;
        } else
        {
            return -1;
        }
    }

    /**
     * *
     *
     * @param caminho este metodo delata o ultimo diretorio descrito na string
     * ex: com/br/teste/teste2 neste caso ele deletará somente o teste2
     * @return
     */
    public boolean deletaDiretorio(String caminho)
    {
        dir = new File(Constantes.caminhoContexto+caminho);
        return dir.delete();
    }
}
