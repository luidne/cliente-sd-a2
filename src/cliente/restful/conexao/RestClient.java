package cliente.restful.conexao;

import cliente.restful.util.Constantes;
import cliente.restful.util.ControlaArquivos;
import cliente.restful.util.ControlaDiretorios;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HeaderElement;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

public class RestClient {

    private String server;
    private static final int timeoutConnection = 3000;
    private static final int timeoutSocket = 5000;

//	private static final String host = "http://sifiscal.com.br/ws-meubusao/";
//	private static final String host = "http://meubusao.jelasticlw.com.br/ws-meubusao/";
    private static final String host = "http://localhost:8080/servidor_de_Musica/webresources/";

    private ObjectMapper mapper;

    public RestClient(String server) {
        this.server = host + server;
        this.mapper = new ObjectMapper();
    }

    private String getBase() {
        return server;
    }

    @SuppressWarnings("finally")
    public File getOrDeleteBaseURI(String uriPath, boolean get) {
        File result = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();

            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpRequestBase getRequest;
            if (get) {
                getRequest = new HttpGet(getBase() + uriPath);
            } else {
                getRequest = new HttpDelete(getBase() + uriPath);
            }
            getRequest.addHeader("accept", "multipart/form-data");
            HttpResponse response = httpClient.execute(getRequest);
            result = getResultFile(response);
            httpClient.getConnectionManager().shutdown();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            return result;
        }
    }

    public File getResultFile(HttpResponse response) {
        File file = null;
        String output = null;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            for (Header hd : response.getAllHeaders()) {
                HeaderElement[] helelms = hd.getElements();
                if (helelms.length > 0) {
                    HeaderElement helem = helelms[0];
                    if (helem.getName().equalsIgnoreCase("attachment")) {
                        NameValuePair nmv = helem.getParameterByName("filename");
                        if (nmv != null) {
                            output = nmv.getValue();
                        }
                    }

                }
            }

            if (output != null) {
                if (ControlaDiretorios.criaDiretorio2("") > 0 ) {
                    InputStream in = response.getEntity().getContent();
                    ControlaArquivos.gravarArquivo(output, in);

                    file = new File(Constantes.caminhoContexto+"/"+output);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return file;
    }

    public File getObject(String strUrl) {
        return getOrDeleteBaseURI(strUrl, true);
    }

    public <T> List<T> getList(String strUrl) {
        try {
            File json = getOrDeleteBaseURI(strUrl, true);
            List<T> list;
            TypeFactory t = TypeFactory.defaultInstance();
            list
                    = mapper.readValue(json, t.constructCollectionType(ArrayList.class, File.class
                            ));
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<T>();
    }

}
