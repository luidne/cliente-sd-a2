package cliente.restful;

import cliente.restful.modelo.RotaDAORest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luidne
 */
public class ClienteRESTful {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        RotaDAORest rotaDAO = new RotaDAORest();
        
        File musica = null;
        List<File> listaDeMusicas = new ArrayList<File>();
        
        System.out.println("Baixando arquivo...\n");
        musica = rotaDAO.buscarPorLinha(1);
        System.out.println("Arquivo baixado.\n");
        
        System.out.println("Nome do arquivo: " +musica.getName());
            System.out.println("Endereço: " +musica.getPath());
            System.out.println("Tamanho: " +musica.length()+" bytes");

        for(File file : listaDeMusicas){
            System.out.println("Nome do arquivo: " +file.getName());
            System.out.println("Endereço: " +file.getAbsolutePath());
            System.out.println("Tamanho: " +file.length()+" bytes");
        }     
    }
    
}
